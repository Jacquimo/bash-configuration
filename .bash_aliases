

#
# General Programs
#
alias dc++='~/../../cygdrive/c/Program\ Files/DC++/DCPlusPlus.exe &'
alias dtella='~/../../cygdrive/c/"Program Files (x86)"/Dtella@Purdue/dtella.exe & dc++'
alias firefox='~/../../cygdrive/c/Program\ Files\ \(x86\)/Mozilla\ Firefox/firefox.exe &'
alias putty='~/../../cygdrive/c/Users/ghouston/Desktop/putty.exe &'
alias cygwin='~/../../cygdrive/c/cygwin64/bin/mintty.exe -i /Cygwin-Terminal.ico &'
alias terminal=cygwin 
alias prompt=cygwin 
alias eclipse='~/../../cygdrive/c/Users/ghouston/Desktop/CS\ Eclipse\ Projects/eclipse/eclipse.exe &'
alias chrome='~/../../cygdrive/c/Program\ Files\ \(x86\)/Google/Chrome/Application/chrome.exe &'
alias vs='~/../../cygdrive/c/Program\ Files\ \(x86\)/Microsoft\ Visual\ Studio\ 10.0/Common7/IDE/devenv.exe &'
alias trillian='~/../../cygdrive/c/Program\ Files\ \(x86\)/Trillian/trillian.exe &'
alias itunes='~/../../cygdrive/c/Program\ Files\ \(x86\)/iTunes/iTunes.exe &'
alias xming='~/../../cygdrive/c/Program\ Files\ \(x86\)/Xming/Xming.exe :0 -clipboard -multiwindow &'
alias tor='~/../../cygdrive/c/Users/ghouston/Desktop/Tor\ Browser/Start\ Tor\ Browser.exe'

#
# Microsoft Office Programs
#
alias onenote='~/../../cygdrive/c/"Program Files (x86)"/Microsoft\ Office/Office14/ONENOTE.exe &' 
alias word='~/../../cygdrive/c/"Program Files (x86)"/"Microsoft Office"/Office14/WINWORD.exe &'
alias powerpoint='~/../../cygdrive/c/"Program Files (x86)"/"Microsoft Office"/Office14/POWERPNT.exe &'
alias pwrpt=powerpoint
alias excel='~/../../cygdrive/c/"Program Files (x86)"/"Microsoft Office"/Office14/EXCEL.exe &'
alias outlook='~/../../cygdrive/c/"Program Files (x86)"/"Microsoft Office"/Office14/OUTLOOK.exe &'

#
# General Microsoft Programs/Files
#
alias calc='~/../../cygdrive/c/Windows/System32/calc.exe &'

#
# Navigation
#
# alias winhome='cd ~/../../cygdrive/c/Users/ghouston/'
# alias home='winhome'
# alias gitRepos='cd ~/../../cygdrive/c/Users/ghouston/Desktop/gitRepos/'
# alias repos='gitRepos'
# alias notes='home; cd Desktop/Fall\ 2013\ Lecture\ Notes/'
# alias homework='cd ~/../../cygdrive/c/Users/ghouston/Desktop/"CS Eclipse Proj# ects"/Homework'

#
# General Commands
#
# alias aliases='cd; emacs .bash_aliases'
alias h='history'
alias p='pwd'
# alias bashrc='cd; emacs .bashrc'
alias open='cygstart'
# alias gitconfig='cd; emacs .gitconfig'
alias e='emacs'